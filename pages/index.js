import React from 'react'
import { connect } from 'react-redux'
import { incrementCount, decrementCount, resetCount } from '../redux/actions/counter'
import Counter from '../components/Counter/index'

function Index (props) {
  return <Counter {...props} />
}
const mapDispatchToProps = (dispatch) => ({
  incrementCount () {
    dispatch(incrementCount())
  },
  decrementCount () {
    dispatch(decrementCount())
  },
  resetCount () {
    dispatch(resetCount())
  }
})

const mapStateToProps = (state) => ({
  counter: state.counter
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index)
